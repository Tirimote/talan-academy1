import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatureAdminComponent } from './candidature-admin.component';

describe('CandidatureAdminComponent', () => {
  let component: CandidatureAdminComponent;
  let fixture: ComponentFixture<CandidatureAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CandidatureAdminComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CandidatureAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
