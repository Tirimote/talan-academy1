import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { saveAs } from 'file-saver';
import { PrimeIcons, PrimeNGConfig } from 'primeng/api';
import { Table } from 'primeng/table';
import * as XLSX from 'xlsx';
import { DashboardService } from '../service/dashboard.service';

@Component({
  selector: 'app-candidature-admin',
  templateUrl: './candidature-admin.component.html',
  styleUrls: ['./candidature-admin.component.scss'],
})
export class CandidatureAdminComponent implements OnInit {
  search: any;
  candidatures: any = [];
  loading: boolean = true;
  cursusList: any = [];
  statusList: any = [];
  @ViewChild('dt') dt!: Table;

  productDialog!: boolean;
  events1!: any[];
  details: any;
  header!: string;
  constructor(
    private dashboardService: DashboardService,
    public translateService: TranslateService,
    private config: PrimeNGConfig
  ) {}

  ngOnInit(): void {
    this.getAllCandidates();
    this.translateService
      .get('primeng')
      .subscribe((res) => this.config.setTranslation(res));
  }

  getAllCandidates() {
    this.dashboardService.getAllCandidatures().subscribe((data) => {
      let cursusList = new Set();
      let statusList = new Set();
      this.candidatures = data;
      this.loading = false;
      this.candidatures.forEach((candidature: any) => {
        candidature.datePostule = new Date(candidature.datePostule);
        candidature.status = this.translateService.instant(candidature.status);
        cursusList.add(candidature.cursus.name);
        statusList.add(candidature.status);
      });
      this.cursusList = [...cursusList].map((cursus: any) => {
        return { name: cursus, value: cursus };
      });
      this.statusList = [...statusList].map((status: any) => {
        return { name: status, value: status };
      });
    });
  }

  downloadCV(id: number, cv: string) {
    this.dashboardService.getStudentSessionCv(id).subscribe((data) => {
      saveAs(data.body as Blob, cv);
    });
  }

  exportExcel() {
    let element = this.dt.el.nativeElement
    
    let ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

    let wscols: XLSX.ColInfo[] | undefined = [];
  
    // Delete the 'actions' column from the worksheet
    let s = XLSX.utils.sheet_to_json(ws, { header: 0, blankrows: false });
    s.forEach((row: any) => {
      if (Array.isArray(row)) {
        row.splice(10, 1);
      }      
    });
    ws = XLSX.utils.json_to_sheet(s);

    ws['!cols'] = wscols;
 
    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
 
    /* save to file */  
    XLSX.writeFile(wb, 'candidatures-' + new Date().getFullYear()+ '-' + (new Date().getMonth()+1) + '-' + new Date().getDate() + '.xlsx');
  }

  timeLineApplication(candidature: any) {
    this.details = candidature;
    console.log(candidature);
    
    this.productDialog = true;
    this.header =
      'TimeLine Candidature ' +
      candidature.candidat.firstName
        .toLowerCase()
        .replace(/(^|\s)\S/g, (L: any) => L.toUpperCase()) +
      ' ' +
      candidature.candidat.lastName
        .toLowerCase()
        .replace(/(^|\s)\S/g, (L: any) => L.toUpperCase());   
      if (candidature.status == "Acceptée") {  
        this.events1 = [
          {
            status: 'Inscription',
            date: candidature.candidat.registrationDate,
            icon: PrimeIcons.ID_CARD,
            color: '#9C27B0',
          },
          {
            status: 'Candidature',
            date: candidature.datePostule,
            icon: PrimeIcons.FILE_EDIT,
            color: '#673AB7',
          },
          {status: 'Entretien', date: new Date(), icon: PrimeIcons.PHONE, color: '#FF9800'},
            {status: 'Acceptation', date: new Date(), icon: PrimeIcons.CHECK, color: '#607D8B'}
        ];
      } else {

        this.events1 = [
          {
            status: 'Inscription',
            date: candidature.candidat.registrationDate,
            icon: PrimeIcons.ID_CARD,
            color: '#9C27B0',
          },
          {
            status: 'Candidature',
            date: candidature.datePostule,
            icon: PrimeIcons.FILE_EDIT,
            color: '#673AB7',
          }
        ];
      }
  }

  hideDialog() {
    this.productDialog = false;
  }
}
