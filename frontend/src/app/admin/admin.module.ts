import { CommonModule, registerLocaleData } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import localeFr from '@angular/common/locales/fr';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AngularEmojisModule } from 'angular-emojis';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { RatingModule } from 'primeng/rating';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { SharedModule } from '../shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { CandidatureAdminComponent } from './candidature-admin/candidature-admin.component';
import { CursusAdminComponent } from './cursus-admin/cursus-admin.component';
import { LessonAdminComponent } from './lesson-admin/lesson-admin.component';
import { ModuleAdminComponent } from './module-admin/module-admin.component';
import { SessiondetailsComponent } from './sessiondetails/sessiondetails.component';
import { UsersComponent } from './users/users.component';

// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    ModuleAdminComponent,
    AdminComponent,
    CursusAdminComponent,
    UsersComponent,
    SessiondetailsComponent,
    CandidatureAdminComponent,
    LessonAdminComponent,
  ],
  imports: [
    AngularEditorModule,
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    TableModule,
    InputTextModule,
    FormsModule,
    TabViewModule,
    ButtonModule,
    RatingModule,
    DropdownModule,
    MultiSelectModule,
    ContextMenuModule,
    DialogModule,
    ButtonModule,
    ToastModule,
    AngularEmojisModule,
    TranslateModule.forChild({
      defaultLanguage: 'fr',
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http, './assets/i18n/', '.json');
        },
        deps: [HttpClient],
      },
    }),
  ],
  providers: [MessageService, ConfirmationService, {provide: LOCALE_ID, useValue: 'fr' }
],
})
export class AdminModule {}
